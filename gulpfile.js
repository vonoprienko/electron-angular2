var gulp = require('gulp');
var gulpCopy = require('gulp-copy');
gulp.task('default', copyFunction);

function copyFunction() {
    return gulp
        .src(['node_modules/core-js/client/shim.min.js', 'node_modules/zone.js/dist/zone.js',
            'node_modules/reflect-metadata/Reflect.js', 'node_modules/systemjs/dist/system.src.js',
            'node_modules/@angular/core/bundles/core.umd.js', 'node_modules/@angular/common/bundles/common.umd.js',
            'node_modules/@angular/compiler/bundles/compiler.umd.js', 'node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
            'node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js', 'node_modules/@angular/http/bundles/http.umd.js',
            'node_modules/@angular/router/bundles/router.umd.js', 'node_modules/@angular/forms/bundles/forms.umd.js',
            'node_modules/rxjs/*', 'node_modules/angular-in-memory-web-api/bundles/in-memory-web-api.umd.js',
            'node_modules/rxjs/util/*','node_modules/rxjs/symbol/*'])
        .pipe(gulpCopy('app/libs', { prefix: 1 }))
        ;
}
