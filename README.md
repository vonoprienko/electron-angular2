
```bash
# Clone this repository
git clone https://vonoprienko@bitbucket.org/vonoprienko/electron-angular2.git
# Go into the repository
cd electron-quick-start
# Install dependencies
npm install

#Gulp - copy library in project
gulp

#typescript to js
npm install -g typescript
cd app/app
tsc ./*
cd ../..

# Run the app
npm start
``
