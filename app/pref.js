const {remote} = require('electron');
const {Menu, MenuItem} = remote;
const notifier = require('node-notifier');

const menu = new Menu();
menu.append(new MenuItem({label: 'MenuItem1', click() {
  notifier.notify({
    'title': 'Hello',
    'message': 'Радий, що Вм користуєтесь цим додатком!'
  });
}}));
menu.append(new MenuItem({type: 'separator'}));
menu.append(new MenuItem({label: 'MenuItem2', type: 'checkbox', checked: true}));

window.addEventListener('contextmenu', (e) => {
  e.preventDefault();
  menu.popup(remote.getCurrentWindow())
}, false);










