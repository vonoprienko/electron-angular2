import { Component } from '@angular/core';
import any = jasmine.any;

@Component({
  selector: 'my-app',
  template: `
            <div class="wrapper">
              <h1>Вчимо український алфавіт</h1>
              <p class="letter">{{alphabet[position]}}</p>
              <div class="buttons">
                <button class="btn btn-success btn-lg btnPrev" (click)="clickedPrev($event)">Попередня</button><button class="btn btn-warning btn-lg btnNext" (click)="clickedNext($event)">Наступна</button>
              </div>
            </div>
            `,
})
export class AppComponent  {
  position: number;
  alphabet: any;
  // audio: any;

  constructor() {
    this.alphabet = ['А а', 'Б б', 'В в', 'Г г', 'Ґ ґ', 'Д д', 'Е е', 'Є є', 'Ж ж', 'З з', 'И и', 'І і', 'Ї ї', 'Й й', 'К к', 'Л л' , 'М м', 'Н н', 'О о', 'П п', 'Р р', 'С с', 'Т т', 'У у', 'Ф ф', 'Х х', 'Ц ц', 'Ч ч', 'Ш ш', 'Щ щ', 'Ю ю', 'Я я', 'Ь ь'];
    this.position = 0;
    // this.audio = new Audio();
    // this.audio.src = "assets/volshebnaya-sms.mp3";
    // this.audio.load();
  }

  clickedPrev(event: any) {
    (this.position > 0) ? this.position-- : this.position;
    // this.audio.play();
    event.preventDefault();
  }

  clickedNext(event: any) {
    (this.position < 32) ? this.position++ : this.position;
    // this.audio.play();
    event.preventDefault();
  }
}
